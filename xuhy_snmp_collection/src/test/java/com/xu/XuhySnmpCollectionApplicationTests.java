package com.xu;

import com.xu.dto.DeviceInfoDTO;
import com.xu.dto.SnmpMessageDTO;
import com.xu.enums.ManufacturerEnum;
import com.xu.snmp.SnmpDeviceDataAcquisition;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Map;

@SpringBootTest
class XuhySnmpCollectionApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    void test1(){
        ManufacturerEnum[] values = ManufacturerEnum.values();
        for (ManufacturerEnum value : values) {
            System.out.println(value.getManufacturerName());
        }
    }

    @Test
    void test2(){
        String oid = "1.3.6.1.4.1.25506.1.335";
        String[] split = oid.split("\\.");
        System.out.println(split[6]);
    }
}
