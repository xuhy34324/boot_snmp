package com.xu.constants;


public class DeviceConfigOidConstants {
    /**
     * 设备名
     * */
    public static final String sysDescr = ".1.3.6.1.2.1.1.5.0";
    /**
     * 设备时间（从打开设备时开始计算的时间）
     * */
    public static final String sysTime = "1.3.6.1.2.1.1.3.0";
    /**
     * 设备编号
     * */
    public static final String sysObjectID = "1.3.6.1.2.1.1.2.0";
}
