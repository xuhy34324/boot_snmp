package com.xu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XuhySnmpCollectionApplication {

    public static void main(String[] args) {
        SpringApplication.run(XuhySnmpCollectionApplication.class, args);
    }

}
